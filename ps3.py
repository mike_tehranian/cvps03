"""
CS6476 Problem Set 3 imports. Only Numpy and cv2 are allowed.
"""
import cv2
import numpy as np


def center_from_top_left_corner(point, width, height):
    center = (int(np.rint(point[0] + (width / 2))) \
                , int(np.rint(point[1] + (height / 2))))
    return center


def sort_coordinates(coordinates):
    left_ones = sorted(coordinates, key=lambda x: x[0])[:2]
    right_ones = sorted(coordinates, key=lambda x: x[0])[2:4]

    top_left, bottom_left = sorted(left_ones, key=lambda x: x[1])
    top_right, bottom_right = sorted(right_ones, key=lambda x: x[1])

    return (top_left, bottom_left, top_right, bottom_right)


def get_center_from_template(coordinates, template):
    template_w, template_h = template.shape[:2]

    top_left_corner, bottom_left_corner, top_right_corner, bottom_right_corner \
        = sort_coordinates(coordinates)

    top_left = center_from_top_left_corner(top_left_corner, template_w, template_h)
    bottom_left = center_from_top_left_corner(bottom_left_corner, template_w, template_h)
    top_right = center_from_top_left_corner(top_right_corner, template_w, template_h)
    bottom_right = center_from_top_left_corner(bottom_right_corner, template_w, template_h)

    return (top_left, bottom_left, top_right, bottom_right)


def kmeans_cluster_points(points):
    dt = np.dtype('float,float')
    points_np = np.array(points, dtype='float32')

    points_float = np.float32(points_np)

    # Type of termination criteria, max iterations, epsilon
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    K = 4
    retval, best_labels, centers = cv2.kmeans(points_float,
                                    K,
                                    criteria,
                                    10,
                                    cv2.KMEANS_RANDOM_CENTERS # cv2.KMEANS_PP_CENTERS
                                    )

    return retval, best_labels, centers


def denoise_image(image):
    denoised_image = np.copy(image)
    denoised_image = cv2.medianBlur(image, 5)
    #denoised_image = cv2.bilateralFilter(denoised_image,3,1,1)
    denoised_image = cv2.GaussianBlur(denoised_image, (21,21), 18)

    return denoised_image


def euclidean_distance(p0, p1):
    """Gets the distance between two (x,y) points

    Args:
        p0 (tuple): Point 1.
        p1 (tuple): Point 2.

    Return:
        float: The distance between points
    """
    np0 = np.asarray(p0)
    np1 = np.asarray(p1)
    return np.sqrt(np.sum((np0-np1)**2))


def get_corners_list(image):
    """Returns a ist of image corner coordinates used in warping.

    These coordinates represent four corner points that will be projected to
    a target image.

    Args:
        image (numpy.array): image array of float64.

    Returns:
        list: List of four (x, y) tuples
            in the order [top-left, bottom-left, top-right, bottom-right].
    """
    height, width = image.shape[:2]
    return ((0,0), (0, height-1), (width-1, 0), (width-1, height-1))


def find_markers(image, template=None):
    """Finds four corner markers.

    Use a combination of circle finding, corner detection and convolution to
    find the four markers in the image.

    Args:
        image (numpy.array): image array of uint8 values.
        template (numpy.array): template image of the markers.

    Returns:
        list: List of four (x, y) tuples
            in the order [top-left, bottom-left, top-right, bottom-right].
    """

    # MDT need to de-noise image here
    # Check to see if results or features score low and if so, de-noise the image

    # Maybe only do denoising if both template and harris fail,
    # then try again

    #template_hsv_img = cv2.cvtColor(template, cv2.COLOR_BGR2HSV)
    #hsv_img_in = cv2.cvtColor(input_image, cv2.COLOR_BGR2HSV)

    #white_lower = np.array([0,0,200])
    #white_upper = np.array([255,90,255])

    # Create masks for the image and template
    #whitish_mask = cv2.inRange(hsv_img_in, white_lower, white_upper)
    #template_mask = cv2.inRange(template_hsv_img, white_lower, white_upper)
    #cv2.imshow('image_whitish_mask', whitish_mask)
    #cv2.waitKey()

    #import ipdb; ipdb.set_trace()
    input_image = np.copy(image)

    input_image = cv2.cvtColor(input_image, cv2.COLOR_BGR2GRAY)
    template = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)
    coordinates = find_template_match(input_image, template)

    # First try template matching with no scaling and rotation
    if coordinates is not None:
        top_left, bottom_left, top_right, bottom_right = get_center_from_template(
                coordinates, template)
    else:
        print "Template Match failed...using Harris corner"
        # If template fails then use Harris corner detector
        top_left, bottom_left, top_right, bottom_right = find_corner_detector(input_image)


    return (top_left, bottom_left, top_right, bottom_right)


def find_template_match(input_image, template):
    # Both input_image and template are gray-scale (binary) images

    # In case of a color image, template summation in the numerator and each
    # sum in the denominator is done over all of the channels and separate
    # mean values are used for each channel. That is, the function can take
    # a color template and a color image. The result will still be a
    # single-channel image, which is easier to analyze.

    # All the 6 methods for comparison in a list
    # Good: TM_CCOEFF
    # No good: TM_CCORR
    #methods = ['cv2.TM_CCOEFF', 'cv2.TM_CCOEFF_NORMED', 'cv2.TM_CCORR',
                #'cv2.TM_CCORR_NORMED', 'cv2.TM_SQDIFF', 'cv2.TM_SQDIFF_NORMED']

    methods = ['cv2.TM_CCOEFF_NORMED']

    w, h = template.shape
    res = cv2.matchTemplate(input_image, template, cv2.TM_CCOEFF_NORMED)
    #cv2.imshow('res', res)
    #cv2.waitKey()

    #import ipdb; ipdb.set_trace()
    threshold = 0.81
    loc = np.where(res >= threshold)

    if loc[0].size < 4:
        # Maybe I can still salvage the found points with Harris?
        return None

    # Points close to each other, take the average of the points
    # coalesce_points
    retval, best_labels, centers = kmeans_cluster_points(zip(*loc[::-1]))

    #import ipdb; ipdb.set_trace()
    if centers.size < 4:
        # Maybe I can still salvage the found points with Harris?
        return None

    return centers


def find_corner_detector(gray):
    #gray = cv2.cvtColor(input_image, cv2.COLOR_BGR2GRAY)
    #cv2.imshow('dst1', gray)
    #cv2.waitKey()
    gray = np.float32(gray)
    #cv2.imshow('dst1', gray)
    #cv2.waitKey()
    gray = denoise_image(gray)
    #cv2.imshow('dst1', gray)
    #cv2.waitKey()

    kernel = np.ones((5,5), np.uint8)

    # TODO try out cornerEigenValsAndVecs() too
    # block_size, ksize, hyperparameter
    #dst = cv2.cornerHarris(gray, 9, 9, 0.2)
    dst = cv2.cornerHarris(gray, 5, 9, 0.02)
    #cv2.imshow('dst1', dst)
    #cv2.waitKey()

    dilate = cv2.dilate(dst, kernel, iterations=1)
    #cv2.imshow('dilate', dilate)
    #cv2.waitKey()
    erode = cv2.erode(dst, kernel, iterations=1)
    #cv2.imshow('erode', erode)
    #cv2.waitKey()

    opening = cv2.morphologyEx(dst, cv2.MORPH_OPEN, kernel)
    #cv2.imshow('opening', opening)
    #cv2.waitKey()

    dst = opening

    #input_image[dst>0.01*dst.max()] = [0,0,255]

    threshold = 0.01 * dst.max()
    #print "Threshold is {}".format(threshold)
    loc = np.where(dst >= threshold)

    if loc[0].size == 0:
        print "DIDN'T FIND ANY HARRIS CORNERS!"
        return None
        #continue
    retval, best_labels, centers = kmeans_cluster_points(zip(*loc[::-1]))

    for x,y in centers:
        int_x = int(x)
        int_y = int(y)
        #print int_x, int_y
        #cv2.circle(input_image, (int_x, int_y), 2, [0,0,255], 2)
        if False and euclidean_distance((370, 677), (int_x, int_y)) <= 1:
            print "FOUND IT {}".format(block_size)
            cv2.circle(input_image, (int_x, int_y), 2, [0,0,255], 2)
            cv2.imshow('final', input_image)
            if cv2.waitKey(0) & 0xff == 27:
                cv2.destroyAllWindows()
            exit()

    rounded_points = tuple(tuple(point) for point in np.around(centers).astype(int))
    sorted_points = sort_coordinates(rounded_points)

    return sorted_points


def draw_box(image, markers, thickness=1):
    """Draws lines connecting box markers.

    Use your find_markers method to find the corners.
    Use cv2.line, leave the default "lineType" and Pass the thickness
    parameter from this function.

    Args:
        image (numpy.array): image array of uint8 values.
        markers(list): the points where the markers were located.
        thickness(int): thickness of line used to draw the boxes edges.

    Returns:
        numpy.array: image with lines drawn.
    """
    output_image = np.copy(image)

    top_left, bottom_left, top_right, bottom_right = markers
    cv2.line(output_image, top_left, bottom_left, (0,0,255), thickness)
    cv2.line(output_image, top_left, top_right, (0,0,255), thickness)
    cv2.line(output_image, bottom_right, bottom_left, (0,0,255), thickness)
    cv2.line(output_image, bottom_right, top_right, (0,0,255), thickness)

    return output_image


def project_imageA_onto_imageB(imageA, imageB, homography):
    """Projects image A into the marked area in imageB.

    Using the four markers in imageB, project imageA into the marked area.

    Use your find_markers method to find the corners.

    Args:
        imageA (numpy.array): image array of uint8 values.
        imageB (numpy.array: image array of uint8 values.
        homography (numpy.array): Transformation matrix, 3 x 3.

    Returns:
        numpy.array: combined image
    """
    s_rows, s_cols = imageA.shape[:2]
    d_rows, d_cols = imageB.shape[:2]
    source_image = np.copy(imageA)
    dest_image = np.copy(imageB)

    H_inv = np.linalg.inv(homography)

    col_array = np.arange(d_cols)
    row_array = np.arange(d_rows)
    col_tile = np.tile(col_array, d_rows)
    row_tile = np.repeat(row_array, d_cols)
    one_tile = np.tile(1, d_cols*d_rows)

    dest_loc = np.vstack((col_tile, row_tile, one_tile))
    source_loc = np.dot(H_inv, dest_loc)

    for col_index in range(d_cols*d_rows):
        x_d, y_d, _ = dest_loc[:, col_index]
        source_p = source_loc[:, col_index]
        w = source_p[2]
        source_p = source_p / w
        x, y, _ = source_p
        if x >= 0 and x < (s_cols - 1) \
            and y >= 0 and y < (s_rows - 1):
            # TODO use a better rounding technique that weights surrounding pixels
            s_x = int(x)
            s_y = int(y)
            dest_image[y_d, x_d, :] = source_image[s_y, s_x]


    return dest_image


def find_four_point_transform(src_points, dst_points):
    """Solves for and returns a perspective transform.

    Each source and corresponding destination point must be at the
    same index in the lists.

    Do not use the following functions (you will implement this yourself):
        cv2.findHomography
        cv2.getPerspectiveTransform

    Hint: You will probably need to use least squares to solve this.

    Args:
        src_points (list): List of four (x,y) source points.
        dst_points (list): List of four (x,y) destination points.

    Returns:
        numpy.array: 3 by 3 homography matrix of floating point values.
    """
    # Homography matrix
    H = np.zeros((3, 3))
    # 8 equations and 9 unknowns
    A = np.zeros((8, 9))
    # Solution vector of 9 values
    b = np.zeros([0, 0, 0 , 0, 0 ,0 , 0, 0, 0])

    # System of equations:
    #   ax + by + c - gxx' - hyx' - ix'
    #   dx + ey + f - gxy' - hyy' - iy'
    for index, source, destination in zip(range(0, 8, 2), src_points, dst_points):
        x, y = source
        x_p, y_p = destination
        A[index, :] = [x, y, 1, 0, 0, 0, -x*x_p, -y*x_p, -x_p]
        A[index+1, :] = [0, 0, 0, x, y, 1, -x*y_p, -y*y_p, -y_p]

    # Solve using SVD
    u, s, v = np.linalg.svd(A)

    # Change from 1x9 array to 3x3 matrix
    H = v[8, :].reshape(3, 3)

    # Grader wants the bottom right of the homography to be 1
    i = H[2, 2]
    H = H / i

    #transformed_image_1_corners = cv2.perspectiveTransform(image_1_corners,
                                                           #homography)
    return H


def video_frame_generator(filename):
    """A generator function that returns a frame on each 'next()' call.

    Will return 'None' when there are no frames left.

    Args:
        filename (string): Filename.

    Returns:
        None.
    """
    # Todo: Open file with VideoCapture and set result to 'video'. Replace None
    video = cv2.VideoCapture(filename)

    # Do not edit this while loop
    while video.isOpened():
        ret, frame = video.read()

        if ret:
            yield frame
        else:
            break

    # Todo: Close video (release) and yield a 'None' value. (add 2 lines)
    video.release()
    yield None
